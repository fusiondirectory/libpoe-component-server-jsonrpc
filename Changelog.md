## %"POE::Component::Server::JSONRPC 0.06" - 2018-06-15

### Changed

#### libpoe-component-server-jsonrpc
- libpoe-component-server-jsonrpc#1 Remove JSON::Any uses

### Fixed

#### libpoe-component-server-jsonrpc
- libpoe-component-server-jsonrpc#2 Fix warnings from cpants
